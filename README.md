Usage:

```bash
bundle install
chmod +x ./src/parser.rb
./src/parser.rb <log path>
```

Run tests:

```
bundle exec rake spec
```