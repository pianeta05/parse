require 'spec_helper'
require_relative '../src/website_list.rb'
require 'byebug'

describe 'WebsiteList' do
  before :each do
    @path = 'spec/fixtures/webserver_spec.log'
    @non_existent_path = 'spec/webserver_spec.log'
    @parse =
            {'/contact'    => ['999.999.999', '999.888.777'],
            '/contact/3'  => [
              '444.444.444.444', 
              '444.444.444.444', 
              '444.444.444.444', 
              '444.444.444.445', 
              '444.444.444.446',
              '200.300.400', 
              '400.300.200'
            ],
            '/home'       => [
              '111.111.111.111', 
              '111.111.111.111', 
              '111.111.111.111', 
              '111.111.111.111', 
              '888.777.666', 
              '666.777.888'
            ],
            '/index'      => [
              '333.333.333.333', 
              '333.333.333.333', 
              '222.111.000', 
              '222.111.001',
              '000.111.222'
            ]
        }
  end

  describe '#parse' do
    it 'should receive the path of the log file from the command line' do
      create_websiteList
      expect(@list.parse(@path)).to eq(@parse)
    end

    it "should show an error when path does not exit" do
      create_websiteList
      expect{@list.parse(@non_existent_path)}.to raise_error
    end
  end

  describe '#most_page_views' do
    before{
      @websites_ordered_by_visit =
        [["/contact/3", 7], ["/home", 6], ["/index", 5], ["/contact", 2]]
      }

    it 'should show the websites ordered by visits' do
      create_websiteList
      list_parse
      expect(@list.most_page_views).to eq(@websites_ordered_by_visit)
    end
  end

  describe '#most_unique_page_views' do
    before {
      @websites_ordered_by_unique_pages =
      ["/contact/3", 5], ["/index", 4], ["/home", 3], ["/contact", 2]
    }

    it 'should show the websites ordered by unique pages ' do
      create_websiteList
      list_parse
      expect(@list.most_unique_page_views).to eq(@websites_ordered_by_unique_pages)
    end
  end

    def create_websiteList
      @list = WebsiteList.new
    end

    def list_parse
      @list.parse(@path)
    end
end
