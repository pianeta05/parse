#!/usr/bin/env ruby
require 'byebug'
require_relative './website_list'

if $0 == __FILE__

  # Read log file path from command line arguments
  raise ArgumentError, "Usage: #{$0} log_path" unless ARGV.length == 1
  file = ARGV[0]
  ARGV.clear
  # Parse websites list from file
  list = WebsiteList.new
  if File.file?(file)
    list.parse(file)
    print "\n"
    # Output results
    puts "Most page views"
    list.most_page_views.each do |website|
      puts "#{website[0]} #{website[1]}" + " visits"
    end
    print "\n"
    puts "Most unique page views"
    list.most_unique_page_views.each do |website|
      puts "#{website[0]} #{website[1]}" + " unique views"
    end
  else
    puts "Error No such file or directory @ #{file}"
  end
end
