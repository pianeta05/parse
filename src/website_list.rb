require 'byebug'

class WebsiteList
  def initialize
    @websites = Hash.new(0)
    @counts = Hash.new(0)
    @counts_unique_pageviews = Hash.new(0)
  end

  # Populate @list from a log file in the provided path
  def parse(path)
    @path = path
    @websites = File.foreach(@path).with_object(Hash.new { |h,k| h[k] = [] }) do |line,website|
      url, ip = line.gsub(/\s+/m, ' ').strip.split(" ")
      unless website[url].include?(ip)
        @counts_unique_pageviews[url] += 1
      end
      website[url] << ip
    end
    counts_pageviews
    @websites
  end

  def most_page_views
    @counts.sort_by do |key,value|
      -value
    end
  end

  def most_unique_page_views
    @counts_unique_pageviews.sort_by do |key, value|
      -value
    end
  end

  private
    #Count the websites and order them by most views page
      def counts_pageviews
        @websites.each_with_object({}) do |(url, ips),list|
          @counts[url] = ips.size
        end
      end
end
